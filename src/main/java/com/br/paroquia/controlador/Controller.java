/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.controlador;

/**
 *
 * @author Felipe
 */
public class Controller {
    
    public static Controller instance = null;
    
    private Object bean;
    private Object obj;
    private Object sessao;
    
    private Controller(){
    }
    
    public static Controller getInstance(){
        if(instance == null){
            return instance = new Controller();
        }
        return instance;
    }
    
    public void setBean(Object o){
        this.bean = o;
    }
    
    public Object getBean(){
        return bean;
    }
    
    public void setSessao(Object o){
        this.sessao = o;
    }
    
    public Object getSessao(){
        return sessao;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
    
}
