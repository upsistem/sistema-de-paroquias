/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.util;

/**
 *
 * @author Felipe
 */
public class QuerysNomeadas {

    public static final String LISTAR_ASSISTENTES_ORDEM = "listar.assistente.ord";
    
    public static final String LISTAR_PROFISSAO_ORDEM = "listar.profissao.ord";
    
    public static final String LISTAR_MISSIONARIO_ORDEM = "listar.missionario.ord";
    
    public static final String LISTAR_BATISMO_ORDEM = "listar.batismo.ord";
    
    public static final String LISTAR_DIZIMISTA_ORDEM = "listar.dizimista.ord";
    
    public static final String LISTAR_CASAMENTO_ORDEM = "listar.casamento.ord";
    
    public static final String BUSCAR_ASSISTENTE_LOGIN = "buscar.assistente.login";
    
    public static final String LOGIN_MISSIONARIO_DISPONIVEL = "login.disponivel";
    
    public static final String EMAIL_MISSIONARIO_DISPONIVEL = "email.disponivel";
        
    public static final String LISTAR_DIZIMO_ORDEM = "listar.dizimo.ord";
    
    public static final String EXITE_DIZIMO_CADASTRADO = "existe.dizimo.cadastrado";
    
    public static final String LOGIN_ASSISTENTE = "login.assistente";
    
    public static final String LOGIN_MISSIONARIO = "login.missionario";
    
    public static final String ANIVERSARIO_DIZIMISTA_SEMANA = "listar.aniversariantes.por.periodo";
    
}
