/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Felipe
 */
public class GeradorDeRelatorios {

    public static void gerarRelatorio(String fileName, List lista) throws JRException, FileNotFoundException, IOException {
        
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(lista);
        JasperPrint print = JasperFillManager.fillReport(getCaminho(fileName), null, dataSource);
        JasperViewer view = new JasperViewer(print, false);
        view.setVisible(true);
        
    }
    
    private static String getCaminho(String caminhoResource){
        
        URL url = Thread.currentThread().getClass().getResource(caminhoResource.replace("\\", "/"));
        return url.toString().replace("file:/", "");
    }
    
}
