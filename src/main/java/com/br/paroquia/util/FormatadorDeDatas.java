/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.util;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Years;

/**
 *
 * @author Felipe de Brito Lira
 */
public class FormatadorDeDatas {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static Date formatarDataEntrada(String data) throws ParseException {
        java.util.Date dat = sdf.parse(data);
        return new Date(dat.getTime());
    }

    public static String formatarDataSaida(Date data) {
        return sdf.format(data);
    }

    public static String formatarHorasSaida(Date time) {

        StringBuilder builder = new StringBuilder();
        builder.append(parteHora(time.getHours())).append(":").append(parteHora(time.getMinutes())).append(":").append(parteHora(time.getSeconds()));
        return builder.toString();

    }

    private static String parteHora(int num) {
        if (num < 10) {
            return "0" + String.valueOf(num);
        } else {
            return String.valueOf(num);
        }
    }

    public static Date formatarDataEHoraEntrada(String data, String hora) {
        String[] vetorData = data.split("/");
        String[] vetorHora = hora.split(":");
        return new Date(converte(vetorData[2]), converte(vetorData[1]), converte(vetorData[0]),
                converte(vetorHora[0]), converte(vetorHora[1]), converte(vetorHora[2]));
    }

    private static int converte(String num) {
        return Integer.parseInt(num);
    }

    public static int calcularIdade(String data) throws ParseException {
        
        DateTime dataAtual = new DateTime();
        DateTime dataAniversario = new DateTime(FormatadorDeDatas.formatarDataEntrada(data).getTime());
        
        return Years.yearsBetween(dataAniversario, dataAtual).getYears();
        
    }
    
}
