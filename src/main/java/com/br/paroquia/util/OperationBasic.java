/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Radams Venceslau
 */
public class OperationBasic {

    public static void openDialog(Component component, Dimension tamanhoFrame, Point loc) {
        JDialog dialog = (JDialog) component;
        java.awt.Dimension tamanhoDialogo = dialog.getPreferredSize();
        dialog.setLocation((tamanhoFrame.width - tamanhoDialogo.width) / 2 + loc.x, (tamanhoFrame.height - tamanhoDialogo.height) / 2 + loc.y);
        dialog.setModal(true);
        dialog.pack();
        dialog.setVisible(true);
    }

    public static void habilita(Component[] components, Boolean arg) {
        for (Component component : components) {
            if ((component instanceof JButton) || (component instanceof javax.swing.JTextArea) || (component instanceof JCheckBox) || (component instanceof javax.swing.JTextField) || (component instanceof javax.swing.JComboBox) || (component instanceof javax.swing.JLabel) || (component instanceof JTable) || (component instanceof javax.swing.JFormattedTextField) || (component instanceof javax.swing.JPasswordField) || (component instanceof javax.swing.JTextPane)) {
                component.setEnabled(arg);
            } else if (component instanceof Container) {
                habilita(((Container) component).getComponents(), arg);
            }
        }
    }

    public static boolean validaCampos(Component[] components) {

        for (Component component : components) {
            if (component instanceof javax.swing.JTextArea) {
                JTextArea c = (JTextArea)component;
                if(c.getText().equals("")){
                    c.setBackground(Color.red);
                    c.requestFocus();
                    return false;
                }
            }
            if (component instanceof javax.swing.JTextField) {
                JTextField c = (JTextField)component;
                if( (c.getText().equals("") || c.getText().trim().isEmpty()) && c.isEnabled() ){
                    c.setBackground(Color.red);
                    c.requestFocus();
                    return false;
                }
            }
            if (component instanceof javax.swing.JPasswordField) {
                JTextField c = (JTextField)component;
                if(c.getText().equals("") ){
                    c.setBackground(Color.red);
                    c.requestFocus();
                    return false;
                }
            }
            
            if( component instanceof javax.swing.JFormattedTextField ){
                JFormattedTextField c = (JFormattedTextField)component;
                if(c.getText().equals("")){
                    c.setBackground(Color.red);
                    c.requestFocus();
                    return false;
                }
            }

            if(component instanceof javax.swing.JComboBox){
                JComboBox c = (JComboBox) component;
                if(c.getSelectedItem() == null){
                    c.setBackground(Color.red);
                    c.requestFocus();
                    return false;
                }
            }
//            if(component instanceof Container){
//                if(!(((Container) component).getComponents().length ==0)){
//                  return  validaCampos(((Container) component).getComponents());
//                }else{
//                    continue;
//                }
//            }
            if(component instanceof Container){
                Container container = ((Container) component);
                if( container.getComponents().length != 0){
                    return validaCampos( container.getComponents());
                }else{
                    continue;
                }
            }
        }
        return true;
    }

    public static void limpar(Component[] components) {
        for (Component component : components) {
            if (component instanceof javax.swing.JTextArea) {
                javax.swing.JTextArea textField = (javax.swing.JTextArea) component;
                textField.setText("");
            } else if (component instanceof javax.swing.JFormattedTextField) {
                javax.swing.JFormattedTextField combo = (javax.swing.JFormattedTextField) component;
                combo.setText("");
            } else if (component instanceof javax.swing.JTextField) {
                javax.swing.JTextField text = (javax.swing.JTextField) component;
                text.setText("");
            } else if (component instanceof javax.swing.JPasswordField) {
                javax.swing.JPasswordField pass = (javax.swing.JPasswordField) component;
                pass.setText("");
            } else if (component instanceof javax.swing.JTable) {
                javax.swing.JTable tabela = (javax.swing.JTable) component;
                DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
                dtm.setRowCount(0);
            } else if((component instanceof javax.swing.JTextPane)){
                javax.swing.JTextPane text = (javax.swing.JTextPane) component;
                text.setText("");
            }else if((component instanceof  javax.swing.JTextArea)){
                javax.swing.JTextArea textArea = (javax.swing.JTextArea) component;
                textArea.setText("");
            }
            else if (component instanceof Container) {
                limpar(((Container) component).getComponents());
            } 
        }
    }

    public static void descolorir(Component [] components){
         for (Component component : components) {
            if ( (component instanceof javax.swing.JTextArea) || (component instanceof javax.swing.JTextField) || (component instanceof javax.swing.JFormattedTextField) || (component instanceof javax.swing.JTextPane) ) {
                component.setBackground(Color.WHITE);
            } else if (component instanceof Container) {
                descolorir(((Container) component).getComponents());
            }
        }
    }
}
