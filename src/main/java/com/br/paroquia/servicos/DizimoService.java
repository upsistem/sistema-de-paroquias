/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.beans.Dizimo;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.DizimoException;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.interfaces.InterfaceDizimoService;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class DizimoService implements InterfaceDizimoService{

    InterfaceDaoJPA<Dizimo> dao;
    
    public DizimoService(){
        dao = new DaoGenericoJPA<>();
    }
    
    @Override
    public boolean salvar(Dizimo d) throws DizimoException {
        
        if (d == null) {
            return false;
        }
        
        validar(d);
        
        return dao.gravar(d);
        
    }

    @Override
    public boolean atualizar(Dizimo d) throws DizimoException {
         
        if (d == null) {
            return false;
        }

        validar(d);
        
        return dao.atualizar(d);
    }

    @Override
    public boolean deletar(Dizimo d) throws DizimoException {
        
        if (d == null) {
            return false;
        }

        if (d.getId() <= 0) {
            throw new DizimoException("Não existe dízimo cadastrado com esse ID.");
        }

        return dao.deletar(d);
        
    }

    @Override
    public List<Dizimo> listar() {
        return dao.listar(QuerysNomeadas.LISTAR_DIZIMO_ORDEM);
    }
    
    private void validar(Dizimo d) throws DizimoException {
        
        if (d.getValor() < 0) {
            throw new DizimoException("Valor inválido");
        }
        if (d.getDiaDeVencimento() <= 0 || d.getDiaDeVencimento() >= 32){
            throw new DizimoException("Dia de vencimento inválido");
        }
        
    }

    @Override
    public boolean exiteDizimoCadastrado(int id) {
        
        Map<String, Integer> map = new HashMap<>();
        
        map.put("id", id);
        
        return dao.exiteValorDisponivel(map, QuerysNomeadas.EXITE_DIZIMO_CADASTRADO);
        
    }
    
}
