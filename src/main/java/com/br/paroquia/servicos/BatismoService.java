/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.PK.BatismoPK;
import com.br.paroquia.beans.Batismo;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.BatismoException;
import com.br.paroquia.interfaces.InterfaceBatismoService;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.List;

/**
 *
 * @author Felipe
 */
public class BatismoService implements InterfaceBatismoService {

    private InterfaceDaoJPA<Batismo> dao;

    public BatismoService() {
        dao = new DaoGenericoJPA<>();
    }

    @Override
    public boolean salvar(Batismo b) throws BatismoException {

        if (b == null) {
            return false;
        }

        validar(b);

        return dao.gravar(b);

    }

    @Override
    public boolean atualizar(Batismo b) throws BatismoException {

        if (b == null) {
            return false;
        }

        validar(b);

        return dao.atualizar(b);
    }

    @Override
    public boolean deletar(Batismo b) throws BatismoException {

        if(b == null){
            return false;
        }
        
        validar(b);
        
        return dao.deletar(b);
        
    }

    @Override
    public List<Batismo> listar() {
        return dao.listar(QuerysNomeadas.LISTAR_BATISMO_ORDEM);
    }

    @Override
    public boolean livroDisponivel(String livro, String folha, String batismo) {
        BatismoPK bpk = new BatismoPK(livro, folha, batismo);
        return !dao.existe(bpk, Batismo.class);   
    }

    private void validar(Batismo b) throws BatismoException {

        if (b.getLivro().equals("") || b.getLivro().trim().isEmpty()) {
            throw new BatismoException("Livro inválido.");
        }
        if (b.getFolha().equals("") || b.getFolha().trim().isEmpty()) {
            throw new BatismoException("Folha inválida.");
        }
        if (b.getnBatismo().equals("") || b.getnBatismo().trim().isEmpty()) {
            throw new BatismoException("Batismo inválido.");
        }
        if (b.getDataNascimento() == null) {
            throw new BatismoException("Data de nascimento inválida.");
        }
        if (b.getLocalNascimento().equals("") || b.getLocalNascimento().trim().isEmpty()) {
            throw new BatismoException("Local de nascimento inválido.");
        }
        if (b.getNomeCelebrante().equals("") || b.getNomeCelebrante().trim().isEmpty()) {
            throw new BatismoException("Nome do celebrante inválido.");
        }
        if (b.getNomeBatismo().equals("") || b.getNomeBatismo().trim().isEmpty()) {
            throw new BatismoException("Nome de batismo inválido.");
        }
        if (b.getDataBatismo() == null) {
            throw new BatismoException("Data de batismo inválida.");
        }
        if (b.getPai().equals("") || b.getPai().trim().isEmpty()) {
            throw new BatismoException("Nome do Pai inválido.");
        }
        if (b.getMae().equals("") || b.getMae().trim().isEmpty()) {
            throw new BatismoException("Nome da Mãe inválido.");
        }
        if (b.getPadriho().equals("") || b.getPadriho().trim().isEmpty()) {
            throw new BatismoException("Nome padrinho inválido.");
        }
        if (b.getMadrinha().equals("") || b.getMadrinha().trim().isEmpty()) {
            throw new BatismoException("Nome madrinha inválido.");
        }
    }
}
