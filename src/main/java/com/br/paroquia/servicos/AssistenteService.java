/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.beans.Assistente;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.AssistenteEception;
import com.br.paroquia.interfaces.InterfaceAssistenteService;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class AssistenteService implements InterfaceAssistenteService {

    private InterfaceDaoJPA<Assistente> dao;

    public AssistenteService() {
        this.dao = new DaoGenericoJPA<>();
    }

    @Override
    public boolean salvar(Assistente a) throws AssistenteEception {

        if (a == null) {
            return false;
        }
        
        validar(a);
        
        return dao.gravar(a);

    }

    @Override
    public boolean atualizar(Assistente a) throws AssistenteEception {

        if (a == null) {
            return false;
        }

        validar(a);
        
        return dao.atualizar(a);

    }

    @Override
    public boolean deletar(Assistente a) throws AssistenteEception {

        if (a == null) {
            return false;
        }

        if (a.getId() <= 0) {
            throw new AssistenteEception("Não existe assistente cadastrado com esse ID.");
        }

        return dao.deletar(a);

    }

    @Override
    public List<Assistente> listar() {
        return dao.listar(QuerysNomeadas.LISTAR_ASSISTENTES_ORDEM);
    }

    @Override
    public boolean loginDisponivel(String login) {

        Map<String, String> mapa = new HashMap<>();
        mapa.put("login", login);

        return dao.exiteValorDisponivel(mapa, QuerysNomeadas.BUSCAR_ASSISTENTE_LOGIN);
        
    }

    private void validar(Assistente a) throws AssistenteEception {
        if (a.getNome() == null || a.getNome().equals("") || a.getNome().trim().isEmpty()) {
            throw new AssistenteEception("Nome inválido!");
        }
        if (a.getLogin() == null || a.getLogin().equals("") || a.getLogin().trim().isEmpty()) {
            throw new AssistenteEception("Login inválido!");
        }
        if (a.getSenha() == null || a.getSenha().equals("") || a.getSenha().trim().isEmpty()) {
            throw new AssistenteEception("Senha inválida!");
        }
    }

    @Override
    public Assistente logar(String login, String senha){
        
        Map<String, String> mapa = new HashMap<>();
        mapa.put("login", login);
        mapa.put("senha", senha);
        
        return dao.consultaSimples(mapa, QuerysNomeadas.LOGIN_ASSISTENTE);
        
    }
    
}
