/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.PK.CasamentoPK;
import com.br.paroquia.beans.Casamento;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.CasamentoException;
import com.br.paroquia.interfaces.InterfaceCasamentoService;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.List;

/**
 *
 * @author Felipe
 */
public class CasamentoService implements InterfaceCasamentoService {

    InterfaceDaoJPA<Casamento> dao;

    public CasamentoService() {
        dao = new DaoGenericoJPA<>();
    }

    @Override
    public boolean salvar(Casamento c) throws CasamentoException {

        if (c == null) {
            return false;
        }

        validar(c);

        return dao.gravar(c);

    }

    @Override
    public boolean atualizar(Casamento c) throws CasamentoException {

        if (c == null) {
            return false;
        }

        validar(c);

        return dao.atualizar(c);

    }

    @Override
    public boolean deletar(Casamento c) throws CasamentoException {

        if (c == null) {
            return false;
        }

        validar(c);

        return dao.deletar(c);

    }

    @Override
    public List<Casamento> listar() {
        return dao.listar(QuerysNomeadas.LISTAR_CASAMENTO_ORDEM);
    }

    @Override
    public boolean casamentoDisponivel(String livro, String folha, String batismo) {
        CasamentoPK cpk = new CasamentoPK(livro, folha, batismo);
        return !dao.existe(cpk, Casamento.class);
    }

    private void validar(Casamento b) throws CasamentoException {

        if (b.getLivro().equals("") || b.getLivro().trim().isEmpty()) {
            throw new CasamentoException("Livro inválido.");
        }
        if (b.getFolha().equals("") || b.getFolha().trim().isEmpty()) {
            throw new CasamentoException("Folha inválida.");
        }
        if (b.getnCasamento().equals("") || b.getnCasamento().trim().isEmpty()) {
            throw new CasamentoException("Número do casamento inválido.");
        }
        if (b.getDataDeCasamento() == null) {
            throw new CasamentoException("Data de casamento inválida.");
        }
        if (b.getLocalCasamento().equals("") || b.getLocalCasamento().trim().isEmpty()) {
            throw new CasamentoException("Local de casamento inválido.");
        }
        if (b.getNomeCelebrante().equals("") || b.getNomeCelebrante().trim().isEmpty()) {
            throw new CasamentoException("Nome do celebrante inválido.");
        }
        if (b.getNomeNoivo().equals("") || b.getNomeNoivo().trim().isEmpty()) {
            throw new CasamentoException("Nome do noivo inválido.");
        }
        if (b.getNomeNoiva().equals("") || b.getNomeNoiva().trim().isEmpty()) {
            throw new CasamentoException("Nome da noiva inválido.");
        }
        if (b.getTestemunhaUm().equals("") || b.getTestemunhaUm().trim().isEmpty()) {
            throw new CasamentoException("Nome da testemunha um é inválido.");
        }
        if (b.getTestemunhaDois().equals("") || b.getTestemunhaDois().trim().isEmpty()) {
            throw new CasamentoException("Nome da testemunha dois é inválido.");
        }
    }
}
