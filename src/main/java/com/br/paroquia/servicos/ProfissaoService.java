/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.beans.Profissao;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.ProfissaoException;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.interfaces.InterfaceProfissaoService;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.List;

/**
 *
 * @author Felipe
 */
public class ProfissaoService implements InterfaceProfissaoService{

    InterfaceDaoJPA<Profissao> dao;
    
    public ProfissaoService(){
        this.dao = new DaoGenericoJPA<>();
    }
    
    @Override
    public boolean salvar(Profissao p) throws ProfissaoException {
        
        if(p == null){
            return false;
        }
        
        if(p.getNome().equals("") || p.getNome().trim().isEmpty() ){
            throw new ProfissaoException("Nome inválido!");
        }
        
        return dao.gravar(p);
        
    }

    @Override
    public boolean atualizar(Profissao p) throws ProfissaoException {
        
        if(p == null){
            return false;
        }
        
        if(p.getNome().equals("") || p.getNome().trim().isEmpty() ){
            throw new ProfissaoException("Nome inválido!");
        }
        
        return dao.atualizar(p);
        
    }

    @Override
    public boolean deletar(Profissao p) throws ProfissaoException {
        
        if(p == null){
            return false;
        }
        
        if(p.getId() <= 0){
            throw new ProfissaoException("Não existe profissão cadastrada com esse ID.");
        }
        
        return dao.deletar(p);
        
    }

    @Override
    public List<Profissao> listar() {
        return dao.listar(QuerysNomeadas.LISTAR_PROFISSAO_ORDEM);
    }
    
}
