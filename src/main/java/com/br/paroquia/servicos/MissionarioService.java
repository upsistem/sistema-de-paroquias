/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.beans.Missionario;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.ErroAoDeletar;
import com.br.paroquia.excecao.MissionarioException;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.interfaces.InterfaceMissionarioService;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class MissionarioService implements InterfaceMissionarioService {

    private InterfaceDaoJPA<Missionario> dao;

    public MissionarioService() {
        this.dao = new DaoGenericoJPA<>();
    }

    @Override
    public boolean salvar(Missionario m) throws MissionarioException {

        if (m == null) {
            return false;
        }

        validar(m);

        return dao.gravar(m);

    }

    @Override
    public boolean atualizar(Missionario m) throws MissionarioException {

        if (m == null) {
            return false;
        }

        validar(m);

        return dao.atualizar(m);

    }

    @Override
    public boolean deletar(Missionario m) throws MissionarioException{

        if (m == null) {
            return false;
        }

        if (m.getId() <= 0) {
            throw new MissionarioException("Não exite missionário cadastrado com esse ID.");
        }

        validar(m);

        return dao.deletar(m);

    }

    @Override
    public List<Missionario> listar() {

        return dao.listar(QuerysNomeadas.LISTAR_MISSIONARIO_ORDEM);

    }

    @Override
    public boolean loginDisponivel(String login) {
        
        Map<String, String> map = new HashMap<>();
        map.put("login", login);
        
        return dao.exiteValorDisponivel(map, QuerysNomeadas.LOGIN_MISSIONARIO_DISPONIVEL);
        
    }

    @Override
    public boolean emailDisponivel(String email) {
        
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        
        return dao.exiteValorDisponivel(map, QuerysNomeadas.EMAIL_MISSIONARIO_DISPONIVEL);
        
    }

    private void validar(Missionario m) throws MissionarioException {
        if (m.getNome() == null || m.getNome().equals("") || m.getNome().trim().isEmpty()) {
            throw new MissionarioException("Nome inválido.");
        }
        if (m.getEmail() == null || m.getEmail().equals("") || m.getEmail().trim().isEmpty()) {
            throw new MissionarioException("Email inválido.");
        }
        if (m.getSenha() == null || m.getSenha().equals("") || m.getSenha().trim().isEmpty()) {
            throw new MissionarioException("Senha inválida.");
        }
        if (m.getLogin() == null || m.getLogin().equals("") || m.getLogin().trim().isEmpty()) {
            throw new MissionarioException("Login inválido.");
        }
        if (m.getDataNascimento() == null) {
            throw new MissionarioException("Data de Nascimento inválida.");
        }
        if (m.getTelefone() == null || m.getTelefone().equals("") || m.getTelefone().trim().isEmpty()) {
            throw new MissionarioException("Telefone inválido.");
        }
        if (m.getEndereco() == null) {
            throw new MissionarioException("Endereço inválido.");
        }
        if (m.getEndereco().getRua() == null || m.getEndereco().getRua().equals("") || m.getEndereco().getRua().trim().isEmpty()) {
            throw new MissionarioException("Rua inválida.");
        }
        if (m.getEndereco().getNumero() == null || m.getEndereco().getNumero().equals("") || m.getEndereco().getNumero().trim().isEmpty()) {
            throw new MissionarioException("Número inválido.");
        }
        if (m.getEndereco().getBairro() == null || m.getEndereco().getBairro().equals("") || m.getEndereco().getBairro().trim().isEmpty()) {
            throw new MissionarioException("Bairro inválido.");
        }
        if (m.getEndereco().getCidade() == null || m.getEndereco().getCidade().equals("") || m.getEndereco().getCidade().trim().isEmpty()) {
            throw new MissionarioException("Cidade inválida.");
        }
        if (m.getEndereco().getComunidade() == null || m.getEndereco().getComunidade().equals("") || m.getEndereco().getComunidade().trim().isEmpty()) {
            throw new MissionarioException("Comunidade inválida.");
        }
        if (m.getEndereco().getSetor() == null || m.getEndereco().getSetor().equals("") || m.getEndereco().getSetor().trim().isEmpty()) {
            throw new MissionarioException("Setor inválido.");
        }
    }

    @Override
    public Missionario logar(String login, String senha) {
        
        Map<String, String> mapa = new HashMap<>();
        mapa.put("login", login);
        mapa.put("senha", senha);
        
        return dao.consultaSimples(mapa,QuerysNomeadas.LOGIN_MISSIONARIO);
        
    }
}
