/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.servicos;

import com.br.paroquia.beans.Dizimista;
import com.br.paroquia.beansVO.AniversarioVO;
import com.br.paroquia.dao.DaoGenericoJPA;
import com.br.paroquia.excecao.DizimistaException;
import com.br.paroquia.interfaces.InterfaceDaoJPA;
import com.br.paroquia.interfaces.InterfaceDizimitaService;
import com.br.paroquia.util.QueriesNativas;
import com.br.paroquia.util.QuerysNomeadas;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;

/**
 *
 * @author Felipe
 */
public class DizimistaService implements InterfaceDizimitaService {

    private InterfaceDaoJPA<Dizimista> dao;

    public DizimistaService() {
        this.dao = new DaoGenericoJPA<>();
    }

    @Override
    public boolean salvar(Dizimista d) throws DizimistaException {

        if (d == null) {
            return false;
        }

        validar(d);

        return dao.gravar(d);

    }

    @Override
    public boolean atualizar(Dizimista d) throws DizimistaException {

        if (d == null) {
            return false;
        }

        validar(d);

        return dao.atualizar(d);
    }

    @Override
    public boolean deletar(Dizimista d) throws DizimistaException {

        if (d == null) {
            return false;
        }

        if (d.getId() <= 0) {
            throw new DizimistaException("Não exite dizimista cadastrado com esse ID.");
        }

        validar(d);

        return dao.deletar(d);

    }

    @Override
    public List<Dizimista> listar() {

        return dao.listar(QuerysNomeadas.LISTAR_DIZIMISTA_ORDEM);

    }

    @Override
    public List aniversariantePorDia(int dia, int mes) {

        if ((dia <= 0 || dia > 31) && (mes <= 0 || mes > 12)) {
            return null;
        }

        String sql = QueriesNativas.ARNIVERSARIO_DIZIMISTA_DIA;
        sql = sql.replace(":dia", String.valueOf(dia)).replace(":mes", String.valueOf(mes));

        return dao.listarComNativeQuery(sql, "aniversariantesVO");

    }

    @Override
    public List aniversariantePorSemana(Date dataInicio, Date dataFim) {
        
        Map<String, Date> mapa = new HashMap<>();
        mapa.put("dataInicio", dataInicio);
        mapa.put("dataFim", dataFim);
        
        return dao.consultaComplexa(mapa, QuerysNomeadas.ANIVERSARIO_DIZIMISTA_SEMANA);
        
    }
    
    @Override
    public List<AniversarioVO> aniversariantePorMes(int mes, int ano) {
        
        int anoAtual = new DateTime(Calendar.getInstance().getTime()).getYear();
        
        if ((mes <= 0 || mes > 12) && (ano < 1900 || ano > anoAtual)) {
            return null;
        }
        
        String sql = QueriesNativas.ARNIVERSARIO_DIZIMISTA_MES;
        sql = sql.replace(":mes", String.valueOf(mes)).replace(":ano", String.valueOf(ano));

        return (List) dao.listarComNativeQuery(sql, "aniversariantesVO");

    }

    private void validar(Dizimista d) throws DizimistaException {
        if (d.getNome() == null || d.getNome().equals("") || d.getNome().trim().isEmpty()) {
            throw new DizimistaException("Nome inválido.");
        }
        if (d.getDataNascimento() == null) {
            throw new DizimistaException("Data de Nascimento inválida.");
        }
        if (d.getTelefone() == null || d.getTelefone().equals("") || d.getTelefone().trim().isEmpty()) {
            throw new DizimistaException("Telefone inválido.");
        }
        if (d.getEndereco() == null) {
            throw new DizimistaException("Endereço inválido.");
        }
        if (d.getEndereco().getRua() == null || d.getEndereco().getRua().equals("") || d.getEndereco().getRua().trim().isEmpty()) {
            throw new DizimistaException("Rua inválida.");
        }
        if (d.getEndereco().getNumero() == null || d.getEndereco().getNumero().equals("") || d.getEndereco().getNumero().trim().isEmpty()) {
            throw new DizimistaException("Número inválido.");
        }
        if (d.getEndereco().getBairro() == null || d.getEndereco().getBairro().equals("") || d.getEndereco().getBairro().trim().isEmpty()) {
            throw new DizimistaException("Bairro inválido.");
        }
        if (d.getEndereco().getCidade() == null || d.getEndereco().getCidade().equals("") || d.getEndereco().getCidade().trim().isEmpty()) {
            throw new DizimistaException("Cidade inválida.");
        }
        if (d.getEndereco().getComunidade() == null || d.getEndereco().getComunidade().equals("") || d.getEndereco().getComunidade().trim().isEmpty()) {
            throw new DizimistaException("Comunidade inválida.");
        }
        if (d.getEndereco().getSetor() == null || d.getEndereco().getSetor().equals("") || d.getEndereco().getSetor().trim().isEmpty()) {
            throw new DizimistaException("Setor inválido.");
        }
    }
    
}
