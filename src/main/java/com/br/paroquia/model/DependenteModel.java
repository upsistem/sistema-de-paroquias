/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Dependente;
import com.br.paroquia.beans.Dizimista;
import com.br.paroquia.interfaces.InterfaceTableModel;
import com.br.paroquia.util.FormatadorDeDatas;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class DependenteModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int NOME = 0;
    private static final int TELEFONE = 1;
    private static final int TIPO = 2;
    
    private List<Dependente> listaDependente;
    private String[] colunas;
    private Dependente dependete;
    
    public DependenteModel(List lista){
        this.listaDependente = lista;
        this.colunas = new String[]{"Nome", "Telefone", "Tipo"};
    }
    
    @Override
    public int getRowCount() {
        return listaDependente.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        dependete = listaDependente.get(rowIndex);
        switch(columnIndex){
            case NOME: return dependete.getNome();
            case TELEFONE: return dependete.getTelefone();
            case TIPO: return dependete.getTipo().toString();
            default: return "Não identificado";
        }
    }

    @Override
    public List getList() {
        return listaDependente;
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
}
