/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Casamento;
import com.br.paroquia.interfaces.InterfaceTableModel;
import com.br.paroquia.util.FormatadorDeDatas;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class CasamentoModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int LIVRO = 0;
    private static final int FOLHA = 1;
    private static final int NUMERO = 2;
    private static final int NOIVO = 3;
    private static final int NOIVA = 4;
    private static final int DATA_CASAMENTO = 5;
    
    private List<Casamento> listaCasamento;
    private String[] colunas;
    private Casamento casamento;
    
    public CasamentoModel(List lista){
        this.listaCasamento = lista;
        this.colunas = new String[]{"Livro", "Folha", "Número","Noivo", "Noiva", "Data do Casamento"};
    }
    
    
    @Override
    public int getRowCount() {
        return this.listaCasamento.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        casamento = listaCasamento.get(rowIndex);
        switch(columnIndex){
            case LIVRO: return casamento.getLivro();
            case FOLHA: return casamento.getFolha();
            case NUMERO: return casamento.getnCasamento();
            case NOIVO: return casamento.getNomeNoivo();
            case NOIVA: return casamento.getNomeNoiva();
            case DATA_CASAMENTO: return FormatadorDeDatas.formatarDataSaida( casamento.getDataDeCasamento() );
            default: return "Não identificado";
        }
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
    @Override
    public List getList(){
        return listaCasamento;
    }
    
}
