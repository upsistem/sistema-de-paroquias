/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Profissao;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Felipe
 */
public class ComboBoxProfissaoModel extends AbstractListModel<Object> implements ComboBoxModel<Object> {

    private List<Profissao> lista;
    private Profissao profissao;
    private final static int FIRSTINDEX = 0;
    
    private ComboBoxProfissaoModel(){
        this.lista = new ArrayList<>();
    }
    
    public ComboBoxProfissaoModel( List<Profissao> lista){
        this();
        this.lista.addAll(lista);
        if( getSize() > 0){
            setSelectedItem( null );
        }
    }
    
    @Override
    public void setSelectedItem(Object anItem) {
        profissao = (Profissao)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return profissao;
    }

    @Override
    public int getSize() {
        return this.lista.size();
    }

    @Override
    public Object getElementAt(int index) {
        return this.lista.get(index);
    }

}