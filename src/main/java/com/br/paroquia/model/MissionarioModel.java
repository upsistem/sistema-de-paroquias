/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Missionario;
import com.br.paroquia.interfaces.InterfaceTableModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class MissionarioModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int LOGIN = 0;
    private static final int NOME = 1;
    private static final int RUA = 2;     
    private static final int COMUNIDADE = 3;     
    private static final int BAIRRO = 4;     
    private static final int SETOR = 5;     
    
    private List<Missionario> listaMissionario;
    private String[] colunas;
    private Missionario missionario;
    
    public MissionarioModel(List lista){
        this.listaMissionario = lista;
        this.colunas = new String[]{"Login", "Nome", "Rua", "Comunidade", "Bairro", "Setor"};
    }
    
    @Override
    public int getRowCount() {
        return listaMissionario.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        missionario = listaMissionario.get(rowIndex);
        switch(columnIndex){
            case LOGIN: return missionario.getLogin();
            case NOME: return missionario.getNome();
            case RUA: return missionario.getEndereco().getRua();
            case COMUNIDADE: return missionario.getEndereco().getComunidade();
            case BAIRRO: return missionario.getEndereco().getBairro();
            case SETOR: return missionario.getEndereco().getSetor();
            default: return "Não identificado";
        }
    }

    @Override
    public List getList() {
        return listaMissionario;
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
}
