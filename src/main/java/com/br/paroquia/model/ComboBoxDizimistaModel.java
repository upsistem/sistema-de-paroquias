/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Dizimista;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Felipe
 */
public class ComboBoxDizimistaModel extends AbstractListModel<Object> implements ComboBoxModel<Object> {

    private List<Dizimista> lista;
    private Dizimista dizimista;
    private final static int FIRSTINDEX = 0;
    
    private ComboBoxDizimistaModel(){
        this.lista = new ArrayList<>();
    }
    
    public ComboBoxDizimistaModel( List<Dizimista> lista){
        this();
        this.lista.addAll(lista);
        if( getSize() > 0){
            setSelectedItem( null );
        }
    }
    
    @Override
    public void setSelectedItem(Object anItem) {
        dizimista = (Dizimista)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return dizimista;
    }

    @Override
    public int getSize() {
        return this.lista.size();
    }

    @Override
    public Object getElementAt(int index) {
        return this.lista.get(index);
    }

}