/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Assistente;
import com.br.paroquia.interfaces.InterfaceTableModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class AssistenteModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int LOGIN = 0;
    private static final int NOME = 1;
    private static final int TIPO = 2;     
    
    private List<Assistente> listaAssistente;
    private String[] colunas;
    private Assistente assistente;
    
    public AssistenteModel(List lista){
        this.listaAssistente = lista;
        this.colunas = new String[]{"Login", "Nome", "Tipo"};
    }
    
    
    @Override
    public int getRowCount() {
        return this.listaAssistente.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        assistente = listaAssistente.get(rowIndex);
        switch(columnIndex){
            case LOGIN: return assistente.getLogin();
            case NOME: return assistente.getNome();
            case TIPO: return assistente.getTipo().toString();
            default: return "Não identificado";
        }
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
    @Override
    public List getList(){
        return listaAssistente;
    }
    
}
