/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Dizimo;
import com.br.paroquia.interfaces.InterfaceTableModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class DizimoModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int NOME = 0;
    private static final int VALOR = 1;
    private static final int VENCIMENTO = 2;
    
    private List<Dizimo> listaDizimo;
    private String[] colunas;
    private Dizimo dizimo;
    
    public DizimoModel(List lista){
        this.listaDizimo = lista;
        this.colunas = new String[]{"Nome", "Valor","Dia de Vencimento"};
    }
    
    @Override
    public int getRowCount() {
        return listaDizimo.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        dizimo = listaDizimo.get(rowIndex);
        switch(columnIndex){
            case NOME: return dizimo.getDizimista().getNome();
            case VALOR: return dizimo.getValor();
            case VENCIMENTO: return dizimo.getDiaDeVencimento();
            default: return "Não identificado";
        }
    }

    @Override
    public List getList() {
        return listaDizimo;
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
}
