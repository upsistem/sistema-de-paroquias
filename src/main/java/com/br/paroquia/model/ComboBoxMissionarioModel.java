/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Missionario;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Felipe
 */
public class ComboBoxMissionarioModel extends AbstractListModel<Object> implements ComboBoxModel<Object> {

    private List<Missionario> lista;
    private Missionario missionario;
    private final static int FIRSTINDEX = 0;
    
    private ComboBoxMissionarioModel(){
        this.lista = new ArrayList<>();
    }
    
    public ComboBoxMissionarioModel( List<Missionario> lista){
        this();
        this.lista.addAll(lista);
        if( getSize() > 0){
            setSelectedItem( null );
        }
    }
    
    @Override
    public void setSelectedItem(Object anItem) {
        missionario = (Missionario)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return missionario;
    }

    @Override
    public int getSize() {
        return this.lista.size();
    }

    @Override
    public Object getElementAt(int index) {
        return this.lista.get(index);
    }

}