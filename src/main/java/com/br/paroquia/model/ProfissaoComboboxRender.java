/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Profissao;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 *
 * @author Felipe
 */
public class ProfissaoComboboxRender extends DefaultListCellRenderer{
    
    @Override
    public Component getListCellRendererComponent(JList<? extends Object> list,Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value instanceof Profissao) {
            Profissao profissao = (Profissao) value;
            setText( profissao.getNome() );
        }
        return this;
    }    
    
}
