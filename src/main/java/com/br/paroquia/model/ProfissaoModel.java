/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Profissao;
import com.br.paroquia.interfaces.InterfaceTableModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class ProfissaoModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int ID = 0;
    private static final int NOME = 1;
    
    private List<Profissao> listaProfissao;
    private String[] colunas;
    private Profissao profissao;
    
    public ProfissaoModel(List lista){
        this.listaProfissao = lista;
        this.colunas = new String[]{"ID", "Nome"};
    }
    
    @Override
    public int getRowCount() {
        return this.listaProfissao.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        this.profissao = (Profissao)listaProfissao.get(rowIndex);
        switch(columnIndex){
            case ID: return profissao.getId();
            case NOME: return profissao.getNome();
            default: return "Não identificado";
        }
    }

    @Override
    public List getList() {
        return this.listaProfissao;
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
}
