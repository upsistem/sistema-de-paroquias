/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Anotacao;
import com.br.paroquia.interfaces.InterfaceTableModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class AnotacaoModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int ID = 0;
    private static final int DESCRICAO = 1;
    
    private List<Anotacao> listaAnotacoes;
    private String[] colunas;
    private Anotacao anotacao;
    
    public AnotacaoModel(List lista){
        this.listaAnotacoes = lista;
        this.colunas = new String[]{"Id","Descricao"};
    }
    
    @Override
    public int getRowCount() {
        return listaAnotacoes.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        anotacao = listaAnotacoes.get(rowIndex);
        switch(columnIndex){
            case ID: return anotacao.getId();
            case DESCRICAO: return anotacao.getDescricao();
            default: return "Não identificado";
        }
    }

    @Override
    public List getList() {
        return listaAnotacoes;
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
}
