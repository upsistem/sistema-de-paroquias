/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Assistente;
import com.br.paroquia.beans.Batismo;
import com.br.paroquia.interfaces.InterfaceTableModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class BatismoModel extends AbstractTableModel implements InterfaceTableModel{

    private static final int LIVRO = 0;
    private static final int FOLHA = 1;
    private static final int NUMERO = 2;
    private static final int NOME = 3;
    private static final int PAI = 4;
    private static final int MAE = 5;
    
    private List<Batismo> listaBatismo;
    private String[] colunas;
    private Batismo batismo;
    
    public BatismoModel(List lista){
        this.listaBatismo = lista;
        this.colunas = new String[]{"Livro", "Folha", "Número","Nome", "Pai", "Mãe"};
    }
    
    
    @Override
    public int getRowCount() {
        return this.listaBatismo.size();
    }

    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        batismo = listaBatismo.get(rowIndex);
        switch(columnIndex){
            case LIVRO: return batismo.getLivro();
            case FOLHA: return batismo.getFolha();
            case NUMERO: return batismo.getnBatismo();
            case NOME: return batismo.getNomeBatismo();
            case PAI: return batismo.getPai();
            case MAE: return batismo.getMae();
            default: return "Não identificado";
        }
    }
    
    @Override
    public String getColumnName(int indexColumn){
        return colunas[indexColumn];
    }
    
    @Override
    public List getList(){
        return listaBatismo;
    }
    
}
