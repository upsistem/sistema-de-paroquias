/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.model;

import com.br.paroquia.beans.Dizimista;
import com.br.paroquia.interfaces.InterfaceTableModel;
import com.br.paroquia.util.FormatadorDeDatas;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Felipe
 */
public class DizimistaModel extends AbstractTableModel implements InterfaceTableModel {

    private static final int NOME = 0;
    private static final int MISSIONARIO = 1;
    private static final int NASCIMENTO = 2;
    private static final int CASAMENTO = 3;
    private static final int RUA = 4;
    private static final int COMUNIDADE = 5;
    private static final int BAIRRO = 6;
    private static final int SETOR = 7;
    private List<Dizimista> listaDizimista;
    private String[] colunas;
    private Dizimista dizimista;

    public DizimistaModel(List lista) {
        this.listaDizimista = lista;
        this.colunas = new String[]{"Nome", "Missionário", "Data de Nascimento", "Data de Casamento", "Rua", "Comunidade", "Bairro", "Setor"};
    }

    @Override
    public int getRowCount() {
        return listaDizimista.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        dizimista = listaDizimista.get(rowIndex);
        switch (columnIndex) {
            case NOME:
                return dizimista.getNome();
            case MISSIONARIO:
                return dizimista.getMissionario() == null ? "" : dizimista.getMissionario().getNome();
            case NASCIMENTO:
                return FormatadorDeDatas.formatarDataSaida(dizimista.getDataNascimento());
            case CASAMENTO:
                return dizimista.getDataCasamento() != null ? FormatadorDeDatas.formatarDataSaida(dizimista.getDataCasamento()) : "";
            case RUA:
                return dizimista.getEndereco().getRua();
            case COMUNIDADE:
                return dizimista.getEndereco().getComunidade();
            case BAIRRO:
                return dizimista.getEndereco().getBairro();
            case SETOR:
                return dizimista.getEndereco().getSetor();
            default:
                return "Não identificado";
        }
    }

    @Override
    public List getList() {
        return listaDizimista;
    }

    @Override
    public String getColumnName(int indexColumn) {
        return colunas[indexColumn];
    }
}
