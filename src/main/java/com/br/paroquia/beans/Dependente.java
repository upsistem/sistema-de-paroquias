/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import com.br.paroquia.enumeracao.DependenteType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

/**
 *
 * @author Felipe
 */
@Entity
public class Dependente extends Pessoa implements Serializable{
    
    @Enumerated(EnumType.STRING)
    private DependenteType tipo;
    
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<Dizimista> dizimistas;
    
    public Dependente() {
        super();
        initObjetos();
    }

    public Dependente(DependenteType tipo, String nome, Date dataNascimento, String telefone) {
        super(nome, dataNascimento, telefone);
        initObjetos();
        this.tipo = tipo;
    }

    private void initObjetos(){
        this.dizimistas = new ArrayList<>();
    }
    
    public DependenteType getTipo() {
        return tipo;
    }

    public void setTipo(DependenteType tipo) {
        this.tipo = tipo;
    }

    public List<Dizimista> getDizimistas() {
        return dizimistas;
    }

    public void setDizimistas(List<Dizimista> dizimista) {
        this.dizimistas = dizimista;
    }

    public void addDizimista(Dizimista d){
        this.dizimistas.add(d);
    }

    public boolean removerDizimista(Dizimista d){
        return this.dizimistas.remove(d);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dependente other = (Dependente) obj;
        if (this.tipo != other.tipo) {
            return false;
        }
        if (!Objects.equals(this.dizimistas, other.dizimistas)) {
            return false;
        }
        return true;
    }
    
}
