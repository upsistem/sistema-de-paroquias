/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import com.br.paroquia.beansVO.AniversarioVO;
import com.br.paroquia.enumeracao.EstadoCivilType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@SqlResultSetMapping(
        name = "aniversariantesVO",
        classes = {
    @ConstructorResult(
            targetClass = AniversarioVO.class,
            columns = {
        @ColumnResult(name = "nome", type = String.class),
        @ColumnResult(name = "datanascimento", type = Date.class),
        @ColumnResult(name = "datacasamento", type = Date.class)
    })
})
@NamedQueries(value = {
    @NamedQuery(name = "listar.dizimista.ord", query = "select d from Dizimista d order by d.nome"),
    @NamedQuery(name = "listar.aniversariantes.por.periodo", query = "select new com.br.paroquia.beansVO.AniversarioVO(d.nome, d.dataNascimento, d.dataCasamento) from Dizimista d where (d.dataCasamento between :dataInicio and :dataFim) or (d.dataNascimento between :dataInicio and :dataFim )")
})
@Entity
public class Dizimista extends Pessoa implements Serializable {

    @Enumerated(EnumType.STRING)
    private EstadoCivilType estadoCivil;
    @Temporal(TemporalType.DATE)
    private Date dataCasamento;
    @Embedded
    private Endereco endereco;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    private Profissao profissao;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Missionario missionario;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY, mappedBy = "dizimistas")
    private List<Dependente> dependentes;

    public Dizimista() {
        super();
        initLista();
    }

    public Dizimista(EstadoCivilType estadoCivil, Date dataCasamento, Endereco endereco, Profissao profissao, Missionario missionario, List<Dependente> dependentes, String nome, Date dataNascimento, String telefone) {
        super(nome, dataNascimento, telefone);
        this.estadoCivil = estadoCivil;
        this.dataCasamento = dataCasamento;
        this.endereco = endereco;
        this.profissao = profissao;
        this.missionario = missionario;
        this.dependentes = dependentes;
    }

    public EstadoCivilType getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivilType estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Date getDataCasamento() {
        return dataCasamento;
    }

    public void setDataCasamento(Date dataCasamento) {
        this.dataCasamento = dataCasamento;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public List<Dependente> getDependentes() {
        return dependentes;
    }

    public void setDependentes(List<Dependente> dependentes) {
        this.dependentes = dependentes;
    }

    private void initLista() {
        this.dependentes = new ArrayList<>();
        this.endereco = new Endereco();
        this.profissao = new Profissao();
        this.missionario = new Missionario();
    }

    public void addDependente(Dependente d) {
        this.dependentes.add(d);
    }

    public boolean removerDependente(Dependente d) {
        return this.dependentes.remove(d);
    }

    public Dependente buscarDependente(int index) {
        return this.dependentes.get(index);
    }

    public Missionario getMissionario() {
        return missionario;
    }

    public void setMissionario(Missionario missionario) {
        this.missionario = missionario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.estadoCivil);
        hash = 29 * hash + Objects.hashCode(this.dataCasamento);
        hash = 29 * hash + Objects.hashCode(this.profissao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dizimista other = (Dizimista) obj;
        if (this.estadoCivil != other.estadoCivil) {
            return false;
        }
        if (!Objects.equals(this.dataCasamento, other.dataCasamento)) {
            return false;
        }
        if (!Objects.equals(this.profissao, other.profissao)) {
            return false;
        }
        if (!Objects.equals(this.dependentes, other.dependentes)) {
            return false;
        }
        return true;
    }
}
