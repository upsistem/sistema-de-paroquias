/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

/**
 *
 * @author Felipe
 */
@Entity
@TableGenerator(allocationSize = 1, initialValue = 1, name = "gerar_id_dizimo")
@NamedQueries(value = {
    @NamedQuery(name = "listar.dizimo.ord", query = "select d from Dizimo d order by d.dizimista.nome"),
    @NamedQuery(name = "existe.dizimo.cadastrado", query = "select d from Dizimo d where d.dizimista.id = :id")
})
public class Dizimo implements Serializable {

    @Id
    @GeneratedValue(generator = "gerar_id_dizimo", strategy = GenerationType.TABLE)
    private int id;
    private double valor;
    private int diaDeVencimento;
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Dizimista dizimista;

    public Dizimo() {
        initObjetos();
    }

    public Dizimo(double valor, int diaDeVencimento, Dizimista dizimista) {
        this.valor = valor;
        this.diaDeVencimento = diaDeVencimento;
        this.dizimista = dizimista;
    }

    private void initObjetos() {
        this.dizimista = new Dizimista();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getDiaDeVencimento() {
        return diaDeVencimento;
    }

    public void setDiaDeVencimento(int diaDeVencimento) {
        this.diaDeVencimento = diaDeVencimento;
    }

    public Dizimista getDizimista() {
        return dizimista;
    }

    public void setDizimista(Dizimista dizimista) {
        this.dizimista = dizimista;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.valor) ^ (Double.doubleToLongBits(this.valor) >>> 32));
        hash = 59 * hash + this.diaDeVencimento;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dizimo other = (Dizimo) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.valor) != Double.doubleToLongBits(other.valor)) {
            return false;
        }
        if (this.diaDeVencimento != other.diaDeVencimento) {
            return false;
        }
        return true;
    }
}
