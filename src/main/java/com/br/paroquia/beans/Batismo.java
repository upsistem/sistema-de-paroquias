/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@IdClass(com.br.paroquia.PK.BatismoPK.class)
@NamedQueries(value = {
    @NamedQuery(name = "listar.batismo.ord", query = "select b from Batismo b order by b.livro, b.folha, b.nBatismo, b.nomeBatismo")
})
public class Batismo implements Serializable{

    @Id
    private String livro;
    @Id
    private String folha;
    @Id
    private String nBatismo;
    
    private String nomeBatismo;
    
    @Temporal(TemporalType.DATE)
    private Date dataBatismo;
    
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    
    private String localNascimento;
    private String nomeCelebrante;
    private String pai;
    private String mae;
    private String padriho;
    private String madrinha;
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},fetch = FetchType.LAZY)
    private List<Anotacao> anotacoes;

    public Batismo() {
        initLista();
    }

    public Batismo(String livro, String folha, String nBatismo, String nomeBatismo, Date dataBatismo, Date dataNascimento, String localNascimento, String nomeCelebrante, String pai, String mae, String padriho, String madrinha) {
        this.livro = livro;
        this.folha = folha;
        this.nBatismo = nBatismo;
        this.nomeBatismo = nomeBatismo;
        this.dataBatismo = dataBatismo;
        this.dataNascimento = dataNascimento;
        this.localNascimento = localNascimento;
        this.nomeCelebrante = nomeCelebrante;
        this.pai = pai;
        this.mae = mae;
        this.padriho = padriho;
        this.madrinha = madrinha;
    }

    public String getLivro() {
        return livro;
    }

    public void setLivro(String livro) {
        this.livro = livro;
    }

    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }

    public String getnBatismo() {
        return nBatismo;
    }

    public void setnBatismo(String nBatismo) {
        this.nBatismo = nBatismo;
    }

    public String getNomeBatismo() {
        return nomeBatismo;
    }

    public void setNomeBatismo(String nomeBatismo) {
        this.nomeBatismo = nomeBatismo;
    }

    public Date getDataBatismo() {
        return dataBatismo;
    }

    public void setDataBatismo(Date dataBatismo) {
        this.dataBatismo = dataBatismo;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getLocalNascimento() {
        return localNascimento;
    }

    public void setLocalNascimento(String localNascimento) {
        this.localNascimento = localNascimento;
    }

    public String getPai() {
        return pai;
    }

    public void setPai(String pai) {
        this.pai = pai;
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public String getPadriho() {
        return padriho;
    }

    public void setPadriho(String padriho) {
        this.padriho = padriho;
    }

    public String getMadrinha() {
        return madrinha;
    }

    public void setMadrinha(String madrinha) {
        this.madrinha = madrinha;
    }

    public List<Anotacao> getAnotacoes() {
        return anotacoes;
    }

    public void setAnotacoes(List<Anotacao> anotacoes) {
        this.anotacoes = anotacoes;
    }

    public void addAnotacao(Anotacao a){
        this.anotacoes.add(a);
    }
    
    public void removerAnotacao(Anotacao a){
        this.anotacoes.remove(a);
    }
    
    public Anotacao buscarAnotacao(int index){
        return this.anotacoes.get(index);
    }
    
    private void initLista(){
        this.anotacoes = new ArrayList<>();
    }

    public String getNomeCelebrante() {
        return nomeCelebrante;
    }

    public void setNomeCelebrante(String nomeCelebrante) {
        this.nomeCelebrante = nomeCelebrante;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.livro);
        hash = 37 * hash + Objects.hashCode(this.folha);
        hash = 37 * hash + Objects.hashCode(this.nBatismo);
        hash = 37 * hash + Objects.hashCode(this.nomeBatismo);
        hash = 37 * hash + Objects.hashCode(this.dataBatismo);
        hash = 37 * hash + Objects.hashCode(this.dataNascimento);
        hash = 37 * hash + Objects.hashCode(this.localNascimento);
        hash = 37 * hash + Objects.hashCode(this.nomeCelebrante);
        hash = 37 * hash + Objects.hashCode(this.pai);
        hash = 37 * hash + Objects.hashCode(this.mae);
        hash = 37 * hash + Objects.hashCode(this.padriho);
        hash = 37 * hash + Objects.hashCode(this.madrinha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Batismo other = (Batismo) obj;
        if (!Objects.equals(this.livro, other.livro)) {
            return false;
        }
        if (!Objects.equals(this.folha, other.folha)) {
            return false;
        }
        if (!Objects.equals(this.nBatismo, other.nBatismo)) {
            return false;
        }
        if (!Objects.equals(this.nomeBatismo, other.nomeBatismo)) {
            return false;
        }
        if (!Objects.equals(this.dataBatismo, other.dataBatismo)) {
            return false;
        }
        if (!Objects.equals(this.dataNascimento, other.dataNascimento)) {
            return false;
        }
        if (!Objects.equals(this.localNascimento, other.localNascimento)) {
            return false;
        }
        if (!Objects.equals(this.nomeCelebrante, other.nomeCelebrante)) {
            return false;
        }
        if (!Objects.equals(this.pai, other.pai)) {
            return false;
        }
        if (!Objects.equals(this.mae, other.mae)) {
            return false;
        }
        if (!Objects.equals(this.padriho, other.padriho)) {
            return false;
        }
        if (!Objects.equals(this.madrinha, other.madrinha)) {
            return false;
        }
        return true;
    }

}
