/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Felipe
 */
@Entity
@NamedQueries(value = {
    @NamedQuery(name = "listar.missionario.ord", query = "select m from Missionario m order by m.nome"),
    @NamedQuery(name = "login.disponivel", query = "select m from Missionario m where m.login like :login"),
    @NamedQuery(name = "email.disponivel", query = "select m from Missionario m where m.email like :email"),
        @NamedQuery(name = "login.missionario", query = "select m from Missionario m where m.login like :login and m.senha like :senha")
})
public class Missionario extends Pessoa implements Serializable {

    @Column(unique = true,nullable = false)
    private String email;
    private String login;
    private String senha;
    
    @Embedded
    private Endereco endereco;

    public Missionario() {
        super();
        initObjetos();
    }

    public Missionario(String email, String login, String senha, Endereco endereco, String nome, Date dataNascimento, String telefone) {
        super(nome, dataNascimento, telefone);
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    private void initObjetos() {
        this.endereco = new Endereco();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.email);
        hash = 97 * hash + Objects.hashCode(this.login);
        hash = 97 * hash + Objects.hashCode(this.senha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Missionario other = (Missionario) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        if (!Objects.equals(this.senha, other.senha)) {
            return false;
        }
        return true;
    }
    
}
