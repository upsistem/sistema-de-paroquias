/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
@Entity
@IdClass(com.br.paroquia.PK.CasamentoPK.class)
@NamedQueries(value = {
    @NamedQuery(name = "listar.casamento.ord", query = "select c from Casamento c order by c.livro, c.folha, c.nCasamento, c.dataDeCasamento")
})
public class Casamento implements Serializable{
        
    @Id
    private String livro;
    @Id
    private String folha;
    @Id
    private String nCasamento;
        
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataDeCasamento;
    
    private String nomeNoivo;
    private String nomeNoiva;
    private String nomeCelebrante;
    private String localCasamento;
    private String testemunhaUm;
    private String testemunhaDois;
    
    @OneToMany(cascade = CascadeType.MERGE)
    private List<Anotacao> anotacoes;
    
    @OneToOne(cascade = CascadeType.ALL)
    private Participante noivo;
    
    @OneToOne(cascade = CascadeType.ALL)
    private Participante noiva;

    public Casamento() {
        initLista();
    }

    public Casamento(String livro, String folha, String nCasamento, Date dataDeCasamento, String nomeNoivo, String nomeNoiva, String nomeCelebrante, String localCasamento, String testemunhaUm, String testemunhaDois) {
        this.livro = livro;
        this.folha = folha;
        this.nCasamento = nCasamento;
        this.dataDeCasamento = dataDeCasamento;
        this.nomeNoivo = nomeNoivo;
        this.nomeNoiva = nomeNoiva;
        this.nomeCelebrante = nomeCelebrante;
        this.localCasamento = localCasamento;
        this.testemunhaUm = testemunhaUm;
        this.testemunhaDois = testemunhaDois;
    }

    public String getLivro() {
        return livro;
    }

    public void setLivro(String livro) {
        this.livro = livro;
    }

    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }

    public String getnCasamento() {
        return nCasamento;
    }

    public void setnCasamento(String nCasamento) {
        this.nCasamento = nCasamento;
    }

    public Date getDataDeCasamento() {
        return dataDeCasamento;
    }

    public void setDataDeCasamento(Date dataDeCasamento) {
        this.dataDeCasamento = dataDeCasamento;
    }

    public String getNomeNoivo() {
        return nomeNoivo;
    }

    public void setNomeNoivo(String nomeNoivo) {
        this.nomeNoivo = nomeNoivo;
    }

    public String getNomeNoiva() {
        return nomeNoiva;
    }

    public void setNomeNoiva(String nomeNoiva) {
        this.nomeNoiva = nomeNoiva;
    }

    public String getLocalCasamento() {
        return localCasamento;
    }

    public void setLocalCasamento(String localCasamento) {
        this.localCasamento = localCasamento;
    }

    public String getTestemunhaUm() {
        return testemunhaUm;
    }

    public void setTestemunhaUm(String testemunhaUm) {
        this.testemunhaUm = testemunhaUm;
    }

    public String getTestemunhaDois() {
        return testemunhaDois;
    }

    public void setTestemunhaDois(String testemunhaDois) {
        this.testemunhaDois = testemunhaDois;
    }

    public List<Anotacao> getAnotacoes() {
        return anotacoes;
    }

    public void setAnotacoes(List<Anotacao> anotacoes) {
        this.anotacoes = anotacoes;
    }

    public Participante getNoivo() {
        return noivo;
    }

    public void setNoivo(Participante noivo) {
        this.noivo = noivo;
    }

    public Participante getNoiva() {
        return noiva;
    }

    public void setNoiva(Participante noiva) {
        this.noiva = noiva;
    }
    
    public void addAnotacao(Anotacao a){
        this.anotacoes.add(a);
    }
    
    public void removerAnotacao(Anotacao a){
        this.anotacoes.remove(a);
    }
    
    public Anotacao buscarAnotacao(int index){
        return this.anotacoes.get(index);
    }
    
    private void initLista(){
        this.anotacoes = new ArrayList<>();
//        this.noivo = new Participante();
//        this.noiva = new Participante();
    }

    public String getNomeCelebrante() {
        return nomeCelebrante;
    }

    public void setNomeCelebrante(String nomeCelebrante) {
        this.nomeCelebrante = nomeCelebrante;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.livro);
        hash = 13 * hash + Objects.hashCode(this.folha);
        hash = 13 * hash + Objects.hashCode(this.nCasamento);
        hash = 13 * hash + Objects.hashCode(this.dataDeCasamento);
        hash = 13 * hash + Objects.hashCode(this.nomeNoivo);
        hash = 13 * hash + Objects.hashCode(this.nomeNoiva);
        hash = 13 * hash + Objects.hashCode(this.nomeCelebrante);
        hash = 13 * hash + Objects.hashCode(this.localCasamento);
        hash = 13 * hash + Objects.hashCode(this.testemunhaUm);
        hash = 13 * hash + Objects.hashCode(this.testemunhaDois);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Casamento other = (Casamento) obj;
        if (!Objects.equals(this.livro, other.livro)) {
            return false;
        }
        if (!Objects.equals(this.folha, other.folha)) {
            return false;
        }
        if (!Objects.equals(this.nCasamento, other.nCasamento)) {
            return false;
        }
        if (!Objects.equals(this.dataDeCasamento, other.dataDeCasamento)) {
            return false;
        }
        if (!Objects.equals(this.nomeNoivo, other.nomeNoivo)) {
            return false;
        }
        if (!Objects.equals(this.nomeNoiva, other.nomeNoiva)) {
            return false;
        }
        if (!Objects.equals(this.nomeCelebrante, other.nomeCelebrante)) {
            return false;
        }
        if (!Objects.equals(this.localCasamento, other.localCasamento)) {
            return false;
        }
        if (!Objects.equals(this.testemunhaUm, other.testemunhaUm)) {
            return false;
        }
        if (!Objects.equals(this.testemunhaDois, other.testemunhaDois)) {
            return false;
        }
        return true;
    }

}
