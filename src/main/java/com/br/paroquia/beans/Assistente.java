/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beans;

import com.br.paroquia.enumeracao.AssistenteType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;

/**
 *
 * @author Felipe
 */
@Entity
@TableGenerator(allocationSize = 1, initialValue = 1, name = "gerar_id_assist")
@NamedQueries(value = {
    @NamedQuery(name = "listar.assistente.ord", query = "select a from Assistente a order by a.nome"),
    @NamedQuery(name = "buscar.assistente.login", query = "select a from Assistente a where a.login like :login"),
    @NamedQuery(name = "login.assistente", query = "select a from Assistente a where a.login like :login and a.senha like :senha")
})
public class Assistente implements Serializable{
    
    @Id
    @GeneratedValue(generator = "gerar_id_assist", strategy = GenerationType.TABLE)
    private int id;
    private String nome;
    
    @Column(unique = false)
    private String login;
    private String senha;
    
    @Enumerated(EnumType.STRING)
    private AssistenteType tipo;

    public Assistente() {
    }

    public Assistente(String nome, String login, String senha, AssistenteType tipo) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    
    public AssistenteType getTipo() {
        return tipo;
    }

    public void setTipo(AssistenteType tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.id;
        hash = 89 * hash + (this.nome != null ? this.nome.hashCode() : 0);
        hash = 89 * hash + (this.login != null ? this.login.hashCode() : 0);
        hash = 89 * hash + (this.senha != null ? this.senha.hashCode() : 0);
        hash = 89 * hash + (this.tipo != null ? this.tipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Assistente other = (Assistente) obj;
        if (this.id != other.id) {
            return false;
        }
        if ((this.nome == null) ? (other.nome != null) : !this.nome.equals(other.nome)) {
            return false;
        }
        if ((this.login == null) ? (other.login != null) : !this.login.equals(other.login)) {
            return false;
        }
        if ((this.senha == null) ? (other.senha != null) : !this.senha.equals(other.senha)) {
            return false;
        }
        if ((this.tipo == null) ? (other.tipo != null) : !this.tipo.equals(other.tipo)) {
            return false;
        }
        return true;
    }
    
}
