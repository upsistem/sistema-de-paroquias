/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.PK;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Felipe
 */
public class CasamentoPK implements Serializable{
    
    private String livro;
    private String folha;
    private String nCasamento;

    public CasamentoPK() {
    }

    public CasamentoPK(String livro, String folha, String nCasamento) {
        this.livro = livro;
        this.folha = folha;
        this.nCasamento = nCasamento;
    }

    public String getLivro() {
        return livro;
    }

    public void setLivro(String livro) {
        this.livro = livro;
    }

    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }

    public String getnCasamento() {
        return nCasamento;
    }

    public void setnCasamento(String nCasamento) {
        this.nCasamento = nCasamento;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.livro);
        hash = 43 * hash + Objects.hashCode(this.folha);
        hash = 43 * hash + Objects.hashCode(this.nCasamento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CasamentoPK other = (CasamentoPK) obj;
        if (!Objects.equals(this.livro, other.livro)) {
            return false;
        }
        if (!Objects.equals(this.folha, other.folha)) {
            return false;
        }
        if (!Objects.equals(this.nCasamento, other.nCasamento)) {
            return false;
        }
        return true;
    }
    
}
