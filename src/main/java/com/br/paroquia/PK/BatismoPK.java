/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.PK;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Felipe
 */
public class BatismoPK implements Serializable{

    private String livro;
    private String folha;
    private String nBatismo;

    public BatismoPK() {
    }

    public BatismoPK(String livro, String folha, String nBatismo) {
        this.livro = livro;
        this.folha = folha;
        this.nBatismo = nBatismo;
    }

    public String getLivro() {
        return livro;
    }

    public void setLivro(String livro) {
        this.livro = livro;
    }

    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }

    public String getnBatismo() {
        return nBatismo;
    }

    public void setnBatismo(String nBatismo) {
        this.nBatismo = nBatismo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.livro);
        hash = 59 * hash + Objects.hashCode(this.folha);
        hash = 59 * hash + Objects.hashCode(this.nBatismo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BatismoPK other = (BatismoPK) obj;
        if (!Objects.equals(this.livro, other.livro)) {
            return false;
        }
        if (!Objects.equals(this.folha, other.folha)) {
            return false;
        }
        if (!Objects.equals(this.nBatismo, other.nBatismo)) {
            return false;
        }
        return true;
    }
    
}
