/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.dao;

import com.br.paroquia.interfaces.InterfaceDaoJPA;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Felipe
 */
public class DaoGenericoJPA<T> implements InterfaceDaoJPA<T> {

    private EntityManager em = Persistence.createEntityManagerFactory("paroquiaPU").createEntityManager();

    @Override
    public boolean gravar(T t) {
        try {
            em.getTransaction().begin();
            em.persist(t);
            em.getTransaction().commit();
            em.close();
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return Boolean.FALSE;
        }
    }

    @Override
    public boolean atualizar(T t) {
        try {
            em.getTransaction().begin();
            em.merge(t);
            em.getTransaction().commit();
            em.close();
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return Boolean.FALSE;
        }
    }

    @Override
    public boolean deletar(T t) {
        try {
            em.getTransaction().begin();
            em.remove(em.merge(t));
            em.getTransaction().commit();
            em.close();
            return Boolean.TRUE;
        } catch (Exception e) {
            em.close();
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    @Override
    public List<T> listar(String query) {
        try {
            List<T> lista = em.createNamedQuery(query).getResultList();
            em.close();
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return null;
        }
    }

    @Override
    public boolean exiteValorDisponivel(Map<?, ?> map, String query) {

        try {

            Query q = em.createNamedQuery(query);

            for (String key : (Set<String>) map.keySet()) {
                q.setParameter(key, map.get(key));
            }

            List o = q.getResultList();

            em.close();

            if (!o.isEmpty()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return false;
        }
    }

    @Override
    public boolean existe(Object id, Class<T> klass) {

        try {

            Object o = em.find(klass, id);

            if (o != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return false;
        }
    }

    @Override
    public T consultaSimples(Map<?, ?> map, String query) {
        try {

            Query q = em.createNamedQuery(query);

            for (String key : (Set<String>) map.keySet()) {

                if (map.get(key) instanceof Date) {
                    q.setParameter(key, (Date) map.get(key), TemporalType.DATE);
                } else {
                    q.setParameter(key, map.get(key));
                }
            }

            Object l = q.getSingleResult();

            em.close();

            return (T) l;

        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return null;
        }
    }

    @Override
    public List listarComNativeQuery(String query, String typeResultSetMap) {

        try {

            List lista = em.createNativeQuery(query, typeResultSetMap).getResultList();

            em.close();

            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return null;
        }

    }

    @Override
    public List consultaComplexa(Map<?, ?> map, String query) {
        try {

            Query q = em.createNamedQuery(query);

            for (String key : (Set<String>) map.keySet()) {

                if (map.get(key) instanceof Date) {
                    q.setParameter(key, (Date) map.get(key), TemporalType.DATE);
                } else {
                    q.setParameter(key, map.get(key));
                }
            }

            List lista = q.getResultList();
            
            em.close();

            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            em.close();
            return null;
        }
    }
}
