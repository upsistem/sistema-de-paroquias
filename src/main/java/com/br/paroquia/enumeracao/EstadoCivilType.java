/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.enumeracao;

/**
 *
 * @author Felipe
 */
public enum EstadoCivilType {
    
    CASADO,
    CASADA,
    SOLTEIRO,
    SOLTEIRA,
    SEPARADO,
    SEPARADA,
    DIVORCIADO,
    DIVORCIADA,
    VIÚVO,
    VIÚVA
        
}
