/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.enumeracao;

/**
 *
 * @author Felipe
 */
public enum DependenteType {
 
    CÔNJUGE,
    FILHO,
    FILHA,
    NETO,
    NETA,
    IRMÃO,
    IRMÃ,
    TIO,
    TIA,
    SOBRINHO,
    SOBRINHA,
    PRIMO,
    PRIMA,
    GENRO,
    SOGRA,
    SOGRO,
    NORA,
    ENTEADO,
    ENTEADA
       
}
