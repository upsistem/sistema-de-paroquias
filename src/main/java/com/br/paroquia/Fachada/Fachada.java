/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.Fachada;

import com.br.paroquia.interfaces.InterfaceAssistenteService;
import com.br.paroquia.interfaces.InterfaceBatismoService;
import com.br.paroquia.interfaces.InterfaceCasamentoService;
import com.br.paroquia.interfaces.InterfaceDizimitaService;
import com.br.paroquia.interfaces.InterfaceDizimoService;
import com.br.paroquia.interfaces.InterfaceMissionarioService;
import com.br.paroquia.interfaces.InterfaceProfissaoService;
import com.br.paroquia.servicos.AssistenteService;
import com.br.paroquia.servicos.BatismoService;
import com.br.paroquia.servicos.CasamentoService;
import com.br.paroquia.servicos.DizimistaService;
import com.br.paroquia.servicos.DizimoService;
import com.br.paroquia.servicos.MissionarioService;
import com.br.paroquia.servicos.ProfissaoService;

/**
 *
 * @author Felipe
 */
public class Fachada {

    public static Fachada instance = null;

    private Fachada() {
    }

    public static Fachada getInstance() {
        if (instance == null) {
            return instance = new Fachada();
        }
        return instance;
    }
    
    public InterfaceAssistenteService assistenteService(){
        return new AssistenteService();
    }
     
    public InterfaceProfissaoService profissaoService(){
        return new ProfissaoService();
    }
    
    public InterfaceMissionarioService missionarioService(){
        return new MissionarioService();
    }
    
    public InterfaceDizimitaService dizimistaService(){
        return new DizimistaService();
    }
    
    public InterfaceDizimoService dizimoService(){
        return new DizimoService();
    }
    
    public InterfaceBatismoService batismoService(){
        return new BatismoService();
    }
    
    public InterfaceCasamentoService casamentoService(){
        return new CasamentoService();
    }
    
    public Object logar(String login, String senha, String tipo){
        if(tipo.equals("Assistente")){
            return assistenteService().logar(login, senha);
        }else{
            return missionarioService().logar(login, senha);
        }
    }
    
}
