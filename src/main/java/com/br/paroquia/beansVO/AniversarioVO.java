/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.beansVO;

import com.br.paroquia.util.FormatadorDeDatas;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Felipe
 */
public class AniversarioVO implements Serializable {

    private String nome;
    private Date dataNascimento;
    private Date dataCasamento;

    public AniversarioVO() {
    }

    public AniversarioVO(String nome, Date dataNascimento, Date dataCasamento) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.dataCasamento = dataCasamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataNascimento() {
        return FormatadorDeDatas.formatarDataSaida( dataNascimento );
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getDataCasamento() {
        if(dataCasamento == null){
            return "";
        }
        return FormatadorDeDatas.formatarDataSaida( dataCasamento );
    }

    public void setDataCasamento(Date dataCasamento) {
        this.dataCasamento = dataCasamento;
    }
}
