/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Profissao;
import com.br.paroquia.excecao.ProfissaoException;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceProfissaoService {

    public boolean salvar(Profissao p) throws ProfissaoException;

    public boolean atualizar(Profissao p) throws ProfissaoException;

    public boolean deletar(Profissao p) throws ProfissaoException;

    public List<Profissao> listar();
}
