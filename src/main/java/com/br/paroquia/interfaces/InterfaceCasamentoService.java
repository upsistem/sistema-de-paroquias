/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Casamento;
import com.br.paroquia.excecao.CasamentoException;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceCasamentoService {

    public boolean salvar(Casamento c) throws CasamentoException;

    public boolean atualizar(Casamento c) throws CasamentoException;

    public boolean deletar(Casamento c) throws CasamentoException;

    public List<Casamento> listar();

    public boolean casamentoDisponivel(String livro, String folha, String batismo);
    
}
