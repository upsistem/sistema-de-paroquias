/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Batismo;
import com.br.paroquia.excecao.BatismoException;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceBatismoService {
    
    public boolean salvar(Batismo b) throws BatismoException;

    public boolean atualizar(Batismo b) throws BatismoException;

    public boolean deletar(Batismo b) throws BatismoException;

    public List<Batismo> listar();

    public boolean livroDisponivel(String livro, String folha, String batismo);
    
    
}
