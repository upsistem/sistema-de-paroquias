/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Dizimo;
import com.br.paroquia.excecao.DizimoException;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceDizimoService {
    
    public boolean salvar(Dizimo d) throws DizimoException;

    public boolean atualizar(Dizimo d) throws DizimoException;

    public boolean deletar(Dizimo d) throws DizimoException;

    public List<Dizimo> listar();
    
    public boolean exiteDizimoCadastrado(int id);
    
}
