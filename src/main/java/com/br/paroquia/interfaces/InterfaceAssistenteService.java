/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Assistente;
import com.br.paroquia.excecao.AssistenteEception;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceAssistenteService {

    public boolean salvar(Assistente a) throws AssistenteEception;

    public boolean atualizar(Assistente a) throws AssistenteEception;

    public boolean deletar(Assistente a) throws AssistenteEception;

    public List<Assistente> listar();

    public boolean loginDisponivel(String login);
    
    public Assistente logar(String login, String senha);
            
}
