/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Dizimista;
import com.br.paroquia.excecao.DizimistaException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceDizimitaService {

    public boolean salvar(Dizimista d) throws DizimistaException;

    public boolean atualizar(Dizimista d) throws DizimistaException;

    public boolean deletar(Dizimista d) throws DizimistaException;

    public List<Dizimista> listar();
    
    public List aniversariantePorDia(int dia, int mes);
    
    public List aniversariantePorSemana(Date dataInicio, Date dataFim);
    
    public List aniversariantePorMes(int mes, int ano);
    
}
