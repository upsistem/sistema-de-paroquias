/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public interface InterfaceDaoJPA<T> {

    public boolean gravar(T t);

    public boolean atualizar(T t);

    public boolean deletar(T t);

    public List<T> listar(String query);

    public boolean exiteValorDisponivel(Map<?, ?> map, String query);
    
    public boolean existe(Object id, Class<T> klass );
    
    public T consultaSimples(Map<?, ?> map, String query);
    
    public List listarComNativeQuery(String query, String tipyResultSetMap);
    
    public List consultaComplexa(Map<?, ?> map, String query);
    
}
