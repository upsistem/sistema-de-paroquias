/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.paroquia.interfaces;

import com.br.paroquia.beans.Missionario;
import com.br.paroquia.excecao.MissionarioException;
import java.util.List;

/**
 *
 * @author Felipe
 */
public interface InterfaceMissionarioService {

    public boolean salvar(Missionario m) throws MissionarioException;

    public boolean atualizar(Missionario m) throws MissionarioException;

    public boolean deletar(Missionario m) throws MissionarioException;

    public List<Missionario> listar();

    public boolean loginDisponivel(String login);
    
    public boolean emailDisponivel(String email);
    
    public Missionario logar(String login, String senha);
    
}
